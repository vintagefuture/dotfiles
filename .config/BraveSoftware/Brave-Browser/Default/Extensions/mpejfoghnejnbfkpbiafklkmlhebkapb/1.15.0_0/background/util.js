;
const DefaultOptions = {
    Initialized: false,
    Debug: false,
    Profiles: {},
    ActiveProfile: null,
    ProviderEnabled: true,
    ProviderDisplay: true,
    Providers: {},
    ProviderNewznab: '',
    RefreshRate: 15,
    InterceptDownloads: true,
    InterceptExclude: '',
    EnableNotifications: false,
    EnableNewznab: true,
    IgnoreCategories: false,
    SimplifyCategories: true,
    DefaultCategory: null,
    OverrideCategory: null,
    ReplaceLinks: false,
    UITheme: ''
};
class Util {
    static getLastError() {
        const error = chrome.runtime.lastError;
        if (error
            // Receiving end not existing isn't really concerning, ignore.
            && !/Receiving end does not exist/i.test(error.message)) {
            console.warn('[Util.sendTabMessage] Last error: ', error.message);
            return error.message;
        }
        return null;
    }
    static sendMessage(message) {
        return new Promise((resolve, reject) => {
            chrome.runtime.sendMessage(message, (response) => {
                const error = Util.getLastError();
                if (error) {
                    reject(error);
                }
                else {
                    // console.info('[Util.sendMessage] Response:', response);
                    resolve(response);
                }
            });
        });
    }
    static sendTabMessage(tabId, message) {
        return new Promise((resolve, reject) => {
            chrome.tabs.sendMessage(tabId, message, (response) => {
                const error = Util.getLastError();
                if (error) {
                    reject(error);
                }
                else {
                    // console.info('[Util.sendTabMessage] Response:', response);
                    resolve(response);
                }
            });
        });
    }
    static setMenuIcon(color = 'green', status = null) {
        return new Promise((resolve, reject) => {
            color = color.toLowerCase();
            if (/^(active|downloading)/i.test(color))
                color = 'green';
            if (/^(inactive|idle|paused|gray)/i.test(color))
                color = 'grey';
            if (!/grey|green|orange/.test(color)) {
                console.warn(`[Util.sendTabMessage] Invalid color: ${color}, ${status}`);
                return resolve();
            }
            chrome.browserAction.setTitle({ title: 'NZB Unity' + (status ? ` - ${status}` : '') });
            const icons = {
                path: {}
            };
            [16, 32, 64].forEach((size) => {
                icons.path[size] = chrome.extension.getURL(`content/images/nzb-${size}-${color}.png`);
            });
            return chrome.browserAction.setIcon(icons, () => {
                const error = Util.getLastError();
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    }
    static getQuery(query = window.location.search) {
        return query
            .replace(/^\?/, '')
            .split('&')
            .reduce((q, i) => {
            const [k, v] = i.split('=');
            q[k] = v;
            return q;
        }, {});
    }
    static getQueryParam(k, def = null) {
        return Util.getQuery()[k] || def;
    }
    // Adapted from https://www.abeautifulsite.net/parsing-urls-in-javascript
    static parseUrl(url) {
        const parser = document.createElement('a');
        let search = null;
        if (!/^https?:\/\//i.test(url)) {
            url = `http://${url}`; // default http
        }
        // Let the browser do the work
        parser.href = url;
        // Convert query string to object
        if (parser.search) {
            search = Util.getQuery(parser.search);
        }
        return {
            protocol: parser.protocol,
            host: parser.host,
            hostname: parser.hostname,
            port: parser.port,
            pathname: parser.pathname,
            hash: parser.hash,
            search
        };
    }
    // Adapted from https://gist.github.com/dineshsprabu/0405a1fbebde2c02a9401caee47fa3f5
    static request(options) {
        return new Promise((resolve, reject) => {
            // Options wrangling
            if (!options.url) {
                reject({
                    status: 0,
                    statusText: 'No URL provided.'
                });
            }
            const method = String(options.method || 'GET').toUpperCase();
            const parsed = Util.parseUrl(options.url);
            const headers = options.headers || {};
            let url = `${parsed.protocol}//${parsed.host}${parsed.pathname}`;
            let search = parsed.search;
            if (options.params || options.files || options.multipart) {
                if (method === 'GET') {
                    // GET requests, pack everything in the URL
                    search = search || {};
                    for (const k in options.params) {
                        search[k] = options.params[k];
                    }
                }
                else if (!options.body) {
                    // Other types of requests, figure out content type if not specified
                    // and build the request body if not provided.
                    const type = headers['Content-Type']
                        || (options.json && 'json')
                        || (options.files && 'multipart')
                        || (options.multipart && 'multipart')
                        || 'form';
                    switch (type) {
                        case 'json':
                        case 'application/json':
                            headers['Content-Type'] = 'application/json';
                            options.body = JSON.stringify(options.params);
                            break;
                        case 'multipart':
                        case 'multipart/form-data':
                            delete headers['Content-Type'];
                            options.body = new FormData();
                            for (let k in options.params) {
                                options.body.append(k, options.params[k]);
                            }
                            for (let k in options.files) {
                                options.body.append(k, new Blob([options.files[k].content], { type: options.files[k].type }), options.files[k].filename);
                            }
                            break;
                        case 'form':
                        case 'application/x-www-form-urlencoded':
                        default:
                            headers['Content-Type'] = 'application/x-www-form-urlencoded';
                            options.body = Util.uriEncodeQuery(options.params);
                    }
                }
            }
            if (search) {
                url = `${url}?${Util.uriEncodeQuery(search)}`;
            }
            // Make the request
            // console.debug({ 'util.request': `${method} ${url}` });
            const xhr = new XMLHttpRequest();
            xhr.open(method, url, true, // async
            options.username || null, options.password || null);
            if (options.username && options.password) {
                xhr.withCredentials = true;
                xhr.setRequestHeader('Authorization', `Basic ${btoa(`${options.username}:${options.password}`)}`);
            }
            for (const k in headers || {}) {
                xhr.setRequestHeader(k, headers[k]);
            }
            xhr.onload = () => {
                // console.debug({ 'util.request.onload': [xhr.status, xhr.response] });
                if (xhr.status >= 200 && xhr.status < 300) {
                    try {
                        return resolve(JSON.parse(xhr.response));
                    }
                    catch (e) {
                        return resolve(xhr.response);
                    }
                }
                return reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            };
            xhr.ontimeout = () => {
                // console.debug({ 'util.request.ontimeout': [xhr.status] });
                return reject({
                    status: xhr.status,
                    statusText: 'Request timed out'
                });
            };
            xhr.onerror = () => {
                // console.debug({ 'util.request.onerror': [xhr.status, xhr.statusText] });
                return reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            };
            xhr.send(options.body);
        });
    }
    static uriEncodeQuery(query) {
        return Object.keys(query)
            .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(query[k])}`)
            .join('&');
    }
    static humanSize(bytes) {
        const i = bytes ? Math.floor(Math.log(bytes) / Math.log(Util.byteMultiplier)) : 0;
        const n = (bytes / Math.pow(Util.byteMultiplier, i)).toFixed(2).replace(/\.?0+$/, '');
        return n + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
    }
    static humanSeconds(seconds) {
        const hours = Math.floor(((seconds % 31536000) % 86400) / 3600);
        const minutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
        seconds = (((seconds % 31536000) % 86400) % 3600) % 60;
        return `${hours}:${minutes}:${seconds}`.replace(/^0+:/, '');
    }
    static ucFirst(s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }
    static trunc(s, n) {
        return (s.length > n) ? `${s.substr(0, n - 1)}&hellip;` : s;
    }
    static simplifyCategory(s) {
        // If category name contains any non-word characters (eg "Lol > Wut")
        // just return the first word (eg "Lol")
        if (/[^\w\s]/.test(s)) {
            [s] = s.split(/\s*[^\w\s]+\s*/i);
        }
        return s.toLowerCase();
    }
}
Util.byteMultiplier = 1024;
Util.Byte = Math.pow(Util.byteMultiplier, 0);
Util.Kilobyte = Math.pow(Util.byteMultiplier, 1);
Util.Megabyte = Math.pow(Util.byteMultiplier, 2);
Util.Gigabyte = Math.pow(Util.byteMultiplier, 3);
// Promisified storage
Util._storage = chrome.storage.local;
Util.storage = {
    get: (keys = null) => {
        return new Promise((resolve, reject) => {
            Util._storage.get(keys, (result) => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                }
                else {
                    resolve(result);
                }
            });
        });
    },
    set: (items) => {
        return new Promise((resolve, reject) => {
            Util._storage.set(items, () => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                }
                else {
                    resolve();
                }
            });
        });
    },
    remove: (key) => {
        return (new Promise((resolve, reject) => {
            Util._storage.remove(key, () => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                }
                else {
                    resolve();
                }
            });
        }));
    },
    clear: () => {
        return (new Promise((resolve, reject) => {
            Util._storage.clear(() => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                }
                else {
                    resolve();
                }
            });
        }));
    }
};
class PageUtil {
    static request(options) {
        options.url = `${window.location.origin}${options.url || ''}`;
        return Util.request(options);
    }
    static requestAndAddFile(filename, category = '', url = window.location.origin, params = {}) {
        // A lot of sites require POST to fetch NZB and follow this pattern (binsearch, nzbindex, nzbking)
        // Fetches a single NZB from a POST request and adds it to the server as a file upload
        return Util.request({ method: 'POST', url, params })
            .then(content => Util.sendMessage({
            'content.addFile': { filename, content, category }
        }));
    }
    static bindAddUrl(options, el, exclusive = false) {
        if (exclusive) {
            $(el).off('click');
        }
        return $(el)
            .on('click', (e) => {
            e.preventDefault();
            console.info(`[NZB Unity] Adding URL: ${options.url}`);
            $(e.target).trigger('nzb.pending');
            Util.sendMessage({ 'content.addUrl': options })
                .then((r) => {
                setTimeout(() => {
                    $(e.target).trigger(r === false ? 'nzb.failure' : 'nzb.success');
                }, 1000);
            });
        });
    }
    static createLink() {
        return $(`
      <a class="NZBUnityLink" title="Download with NZB Unity">
        <img src="${PageUtil.iconGreen}">
      </a>
    `)
            .css({
            cursor: 'pointer',
            display: 'inline-block',
        })
            .on('nzb.pending', (e) => {
            $(e.currentTarget).find('img').attr('src', PageUtil.iconGrey);
        })
            .on('nzb.success', (e) => {
            $(e.currentTarget).find('img').attr('src', PageUtil.iconGreen);
        })
            .on('nzb.failure', (e) => {
            $(e.currentTarget).find('img').attr('src', PageUtil.iconRed);
        });
    }
    static createButton() {
        return $(`
      <button class="NZBUnityDownloadAll"
        title="Download selected items with NZB Unity"
      >
        Download Selected
      </button>
    `)
            .css({
            background: `${PageUtil.backgroundNormal} url(${PageUtil.iconGreen}) no-repeat scroll 4px center`,
            border: '1px solid rgb(19, 132, 150)',
            'border-radius': '4px',
            color: '#fff',
            cursor: 'pointer',
            display: 'inline-block',
            'font-size': '11px',
            'font-weight': 'normal',
            margin: '0 0.5em 0 0',
            padding: '3px 8px 3px 25px',
            'text-shadow': '0 -1px 0 rgba(0,0,0,0.25)',
            'white-space': 'nowrap',
        })
            .on('nzb.pending', (e) => {
            $(e.currentTarget).css({
                'background-color': PageUtil.backgroundPending,
                'background-image': `url(${PageUtil.iconGrey})`,
            });
        })
            .on('nzb.success', (e) => {
            $(e.currentTarget).css({
                'background-color': PageUtil.backgroundNormal,
                'background-image': `url(${PageUtil.iconGreen})`,
            });
        })
            .on('nzb.failure', (e) => {
            $(e.currentTarget).css({
                'background-color': PageUtil.backgroundNormal,
                'background-image': `url(${PageUtil.iconRed})`,
            });
        });
    }
    static createAddUrlLink(options, adjacent = null) {
        // console.log('createAddUrlLink', url, category);
        const link = PageUtil.bindAddUrl(options, PageUtil.createLink())
            .attr('href', options.url)
            .css({
            height: '16px',
            width: '16px',
        });
        if (adjacent) {
            link.insertAfter(adjacent);
        }
        return link;
    }
    static createAddUrlButton(options, adjacent = null) {
        // console.log('createAddUrlLink', url, category);
        const button = PageUtil.bindAddUrl(options, PageUtil.createButton());
        if (adjacent) {
            button.insertAfter(adjacent);
        }
        return button;
    }
}
PageUtil.iconGreen = chrome.extension.getURL('content/images/nzb-16-green.png');
PageUtil.iconGrey = chrome.extension.getURL('content/images/nzb-16-grey.png');
PageUtil.iconOrange = chrome.extension.getURL('content/images/nzb-16-orange.png');
PageUtil.iconRed = chrome.extension.getURL('content/images/nzb-16-reg.png');
PageUtil.backgroundNormal = 'rgb(23, 162, 184)';
PageUtil.backgroundPending = 'rgb(156, 166, 168)';
