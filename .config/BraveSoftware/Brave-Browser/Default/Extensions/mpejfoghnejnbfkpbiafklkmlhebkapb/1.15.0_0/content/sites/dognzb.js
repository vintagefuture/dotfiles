class NZBUnityDognzb {
    constructor() {
        this.replace = false;
        Util.storage.get(['Providers', 'ReplaceLinks'])
            .then((opts) => {
            this.replace = opts.ReplaceLinks;
            let provider = opts.Providers && opts.Providers.dognzb;
            let enabled = provider ? provider.Enabled : true;
            if (enabled) {
                console.info(`[NZB Unity] Initializing 1-click functionality...`);
                this.username = this.getUsername();
                // dognzb uses an ajax filter, watch for dom changes.
                this.observer = new MutationObserver((mutations) => {
                    console.info(`[NZB Unity] Content changed, updating links...`);
                    this.initializeLinks();
                });
                this.observer.observe(document.getElementById('content'), { childList: true });
                this.initializeLinks();
            }
            else {
                console.info(`[NZB Unity] 1-click functionality disabled for this site`);
            }
        });
    }
    getUsername() {
        return $('#label').text().trim();
    }
    getNzbUrl(id) {
        let rsstoken = $('input[name="rsstoken" i]').val();
        return `${window.location.origin}/fetch/${id}/${rsstoken}`;
    }
    initializeLinks() {
        // Any page with a table of downloads (browse, search)
        if ($('tr[id^="row"]').length) {
            $('[onclick^="doOneDownload"]').each((i, el) => {
                let a = $(el);
                let idMatch = a.attr('onclick').match(/\('(\w+)'\)/i);
                let id = idMatch && idMatch[1];
                // Get the category
                let catLabel = a.closest('tr')
                    .find('.label').not('.label-empty').not('.label-important');
                let category = catLabel.text();
                let catSrc = 'default';
                let link = PageUtil.createAddUrlLink({
                    url: this.getNzbUrl(id),
                    category: category
                })
                    .on('nzb.success', (e) => {
                    catLabel.closest('tr')
                        .prepend('<td width="19"><div class="dog-icon-tick"></div></td>');
                });
                console.warn(this.replace);
                if (this.replace) {
                    a.closest('td').attr('width', '20');
                    a.replaceWith(link);
                }
                else {
                    a.closest('td').attr('width', '36');
                    a.css({ float: 'left' });
                    link.insertAfter(a);
                }
            });
            // Create download all buttons
            $('[onclick^="doZipDownload"]').each((i, el) => {
                let getNzbUrl = (id) => { return this.getNzbUrl(id); };
                let button = PageUtil.createButton()
                    .css({ 'margin': '0 0.3em 0 0' })
                    .on('click', (e) => {
                    e.preventDefault();
                    let checked = $('#featurebox .ckbox:checked');
                    if (checked.length) {
                        console.info(`[NZB Unity] Adding ${checked.length} NZB(s)`);
                        button.trigger('nzb.pending');
                        Promise.all(checked.map((i, el) => {
                            let check = $(el);
                            let idMatch = check.closest('tr')
                                .find('[onclick^="doOneDownload"]').attr('onclick').match(/\('(\w+)'\)/i);
                            let id = idMatch && idMatch[1];
                            if (/[a-d0-9]+/.test(id)) {
                                // Get the category
                                let catLabel = check.closest('tr')
                                    .find('.label').not('.label-empty').not('.label-important');
                                let category = catLabel.text();
                                let catSrc = 'default';
                                let options = {
                                    url: getNzbUrl(id),
                                    category: category
                                };
                                console.info(`[NZB Unity] Adding URL`, options);
                                return Util.sendMessage({ 'content.addUrl': options });
                            }
                            else {
                                return Promise.resolve();
                            }
                        }))
                            .then((results) => {
                            setTimeout(() => {
                                if (results.some((r) => { return r === false; })) {
                                    button.trigger('nzb.failure');
                                }
                                else {
                                    button.trigger('nzb.success');
                                }
                            }, 1000);
                        });
                    }
                })
                    .insertBefore($(el));
            });
        }
        else if (window.location.pathname.startsWith('/details')) {
            let id = window.location.pathname.match(/\/details\/(\w+)/i)[1];
            // Get the category
            let category = '';
            let catSrc = 'default';
            let catLabel = $('#details tr:nth-child(3) td:nth-child(2)');
            if (catLabel.length) {
                category = catLabel.text().split(/\s*->\s*/)[0];
            }
            let link = PageUtil.createAddUrlLink({
                url: this.getNzbUrl(id),
                category: category
            });
            if (this.replace) {
                $('[onclick^="doOneDownload"]').replaceWith(link);
                link.css({ padding: '0 0 0 5px' });
                link.append('download');
            }
            else {
                link.appendTo($('#preview .btn-group').closest('tr').find('td:first-child'));
            }
        }
    }
}
$(($) => {
    let nzbIntegration = new NZBUnityDognzb();
});
