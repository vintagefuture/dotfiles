var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class NZBUnityTabulaRasa {
    constructor() {
        this.replace = false;
        this.initialize();
    }
    initialize() {
        return __awaiter(this, void 0, void 0, function* () {
            const { Providers, ReplaceLinks } = yield Util.storage.get(['Providers', 'ReplaceLinks']);
            this.replace = ReplaceLinks;
            const provider = Providers && Providers.tabularasa;
            const enabled = provider ? provider.Enabled : true;
            if (enabled) {
                console.info(`[NZB Unity] Initializing 1-click functionality...`);
                this.apikey = yield this.getApiKey();
                if (this.apikey) {
                    console.info(`Got api key: ${this.apikey}`);
                    return this.initializeLinks();
                }
                else {
                    console.warn('Could not get API key');
                }
            }
            else {
                console.info(`[NZB Unity] 1-click functionality disabled for this site`);
            }
        });
    }
    getApiKey() {
        return __awaiter(this, void 0, void 0, function* () {
            const r = yield PageUtil.request({ url: '/profile' });
            const [match, token] = r.match(/api_token=([a-f0-9]+)/i);
            return token;
        });
    }
    getNzbUrl(id) {
        return `${window.location.origin}/getnzb?id=${id}&api_token=${this.apikey}`;
    }
    initializeLinks() {
        // Create direct download links on individual items
        $('a[href*="/getnzb?id="]').each((i, el) => {
            const a = $(el);
            const [match, id] = a.attr('href').match(/\/getnzb\?id=([\w-]+)/i);
            const url = this.getNzbUrl(id);
            // Get the category
            let category = '';
            let catSrc = 'default';
            if (window.location.pathname.startsWith('/details')) {
                const catLink = $('.tab-content a[href*="/browse/"]').filter(':not([href*="?"])');
                if (catLink.length) {
                    category = catLink.text();
                    catSrc = 'href';
                }
                category = category.split(/[^\w-]/)[0]; // Either "Movies: HD" or "Movies HD"
                const linkOpts = { url, category };
                if (this.replace) {
                    PageUtil.bindAddUrl(linkOpts, a, true);
                }
                else {
                    const link = PageUtil.createAddUrlLink(linkOpts);
                    link
                        .html(`${link.html()} NZB Unity`)
                        .addClass('btn btn-light btn-sm btn-success btn-transparent')
                        .css({ display: '', height: '', width: '', 'margin-left': '-1px' })
                        .insertBefore(a);
                }
            }
            else if (a.attr('role') === 'button') {
                const linkOpts = { url, category: null };
                if (this.replace) {
                    PageUtil.bindAddUrl(linkOpts, a, true);
                }
                else {
                    const link = PageUtil.createAddUrlLink(linkOpts)
                        .css({ float: 'left', margin: '0 0.25em 0 0' });
                    a.parent().parent().find('.release-name').prepend(link);
                }
            }
            else {
                // List
                category = a.closest('tr[id^="guid"]').find('td:nth-child(3)').text();
                catSrc = 'href';
                category = category.split(/[^\w-]/)[0]; // Either "Movies: HD" or "Movies HD"
                const linkOpts = { url, category };
                if (this.replace) {
                    PageUtil.bindAddUrl(linkOpts, a, true);
                }
                else {
                    const link = PageUtil.createAddUrlLink(linkOpts)
                        .css({ margin: '0 0.25em 0 0' })
                        .insertBefore(a);
                }
            }
        });
        // Create download all buttons
        $('.nzb_multi_operations_download').each((i, el) => {
            const getNzbUrl = (id) => this.getNzbUrl(id);
            let button = PageUtil.createButton()
                .text('NZB Unity')
                .addClass('btn btn-sm')
                .css({
                'border-top-right-radius': '0',
                'border-bottom-right-radius': '0',
                'line-heignt': '11px',
                'margin': '0',
            })
                .on('click', (e) => {
                e.preventDefault();
                const checked = $('input[name="table_records"]:checked');
                if (checked.length) {
                    console.info(`[NZB Unity] Adding ${checked.length} NZB(s)`);
                    button.trigger('nzb.pending');
                    Promise.all(checked.map((i, el) => {
                        const check = $(el);
                        const id = check.val();
                        if (/[a-d0-9\-]+/.test(id)) {
                            const linkOpts = { url: getNzbUrl(id) };
                            console.info(`[NZB Unity] Adding URL`, linkOpts);
                            return Util.sendMessage({ 'content.addUrl': linkOpts });
                        }
                        else {
                            return Promise.resolve();
                        }
                    }))
                        .then((results) => {
                        setTimeout(() => {
                            if (results.some(r => r === false)) {
                                button.trigger('nzb.failure');
                            }
                            else {
                                button.trigger('nzb.success');
                            }
                        }, 1000);
                    });
                }
            })
                .insertBefore(el);
        });
    }
}
$(($) => {
    const nzbIntegration = new NZBUnityTabulaRasa();
});
undefined;
