    -- Each screen has its own tag table.
    local names = { "term", "www", "nautilus", "spotify", "sound", "6", "7" }
    local l = awful.layout.suit  -- Just to save some typing: use an alias.
    local layouts = { l.tile, l.tile, l.tile, l.tile, l.tile,
        l.floating, l.tile.left }
    awful.tag(names, s, layouts)
    
    
-- Autostart Applications
awful.spawn.with_shell("compton")
awful.spawn.with_shell("nitrogen --restore")

   awful.key({ modkey },            "r",     function () awful.util.spawn("dmenu_run") end,
              {description = "run prompt", group = "launcher"}),
   awful.key({ modkey },            "c",     function () awful.util.spawn("chromium") end,
              {description = "run prompt", group = "launcher"}),
   awful.key({ modkey },            "",     function () awful.util.spawn("dmenu_run") end,
              {description = "run prompt", group = "launcher"}),
   awful.key({ modkey },            "r",     function () awful.util.spawn("dmenu_run") end,
              {description = "run prompt", group = "launcher"}),


-- Themes define colours, icons, font and wallpapers.
beautiful.init("/home/nedo/.config/awesome/theme.lua")
