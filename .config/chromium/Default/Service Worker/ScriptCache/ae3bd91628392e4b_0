0\r�m��      ����    299(function serviceWorker() {
  importScripts(
    'https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js'
  )
  /* global workbox */
  importScripts('https://cdn.jsdelivr.net/npm/idb@2.1.3/lib/idb.min.js')
  /* global idb */

  self.addEventListener('activate', function(event) {
    console.log('Service Worker Activation...')
    function clearOpaqueResponses(cache) {
      return cache
        .keys()
        .then(
          // We get all cached responses in an object { request, response }
          requestKeys =>
            Promise.all(
              requestKeys.map(request =>
                cache.match(request).then(response => ({ request, response }))
              )
            )
        )
        .then(
          // Then we filter to get only the opaque responses
          responsesObj =>
            responsesObj.filter(({ response }) => !response.status)
        )
        .then(
          // Finally, we delete all opaque responses
          opaqueResponsesObj =>
            Promise.all(
              opaqueResponsesObj.map(({ request }) => cache.delete(request))
            )
        )
    }

    function clearAllOpaqueResponses() {
      return caches
        .keys() // First we get all cache keys to update all caches
        .then(cacheKeys =>
          Promise.all(cacheKeys.map(cacheKey => caches.open(cacheKey)))
        )
        .then(caches => caches.map(clearOpaqueResponses))
    }

    event.waitUntil(clearAllOpaqueResponses())
  })

  self.addEventListener('push', evt => {
    if (!evt.data) {
      return
    }

    let data

    try {
      data = evt.data.json()
    } catch (err) {
      console.log('Error occurred when trying to decode push event', err)
      return
    }

    const promiseChain = self.registration.showNotification(data.title || '', {
      body: data.message,
    })

    evt.waitUntil(promiseChain)
  })

  if (!workbox) {
    console.log("Boo! Workbox didn't load 😬")
    return
  }

  console.log('Yay! Workbox is loaded 🎉')

  const sessionDB = 'session'

  const cacheKeyDB = 'cacheKey'

  const sessionTable = 'session'

  const cacheKeyTable = 'cacheKey'

  const WHITELIST_QS_PARAMS = new Set([
    'utm_source',
    'utm_campaign',
    'utmi_campaign',
    'utmi_cp',
    '__disableSSR',
    'map',
    'order',
    'priceRange',
    'fq',
    'ft',
    'sc',
    'workspace',
    'homescreen',
  ])

  /* functions */
  const handleError = async function(err) {
    console.log(err)
  }

  const handleErrorDB = async function(db, table, err) {
    console.log(
      `Failed to execute query on DB ${db} on table ${table}.`,
      '\\n',
      err
    )
  }

  // Database Functions

  const database = {
    open(dbname, table, keys) {
      return idb.open(dbname, 1, function(upgradeDb) {
        if (!upgradeDb.objectStoreNames.contains(table)) {
          const objectOS = upgradeDb.createObjectStore(table, {
            keyPath: keys,
          })
          objectOS.createIndex(keys, keys, {
            unique: true,
          })
        }
      })
    },
    async get(dbPromise, dbname, table, key) {
      const db = await dbPromise
      const tx = db.transaction(dbname, 'readonly')
      const store = tx.objectStore(table)
      return store.get(key)
    },
    async set(dbPromise, dbname, table, data) {
      const db = await dbPromise
      const tx = db.transaction(dbname, 'readwrite')
      const store = tx.objectStore(table)

      try {
        return await Promise.all(data.map(val => store.put(val)))
      } catch (err) {
        tx.abort()
        throw Error(`Events were not added to the store. ${err}`)
      }
    },
    async close(dbPromise) {
      try {
        const db = await dbPromise
        db.close()
      } catch (err) {
        console.log(
          `Failed to close connection to database ${dbPromise.name}`,
          '\\n',
          err
        )
      }
    },
  }

  // Database Connections

  const dbConnections = {
    session() {
      return database.open(sessionDB, sessionTable, 'name')
    },
    cacheKey() {
      return database.open(cacheKeyDB, cacheKeyTable, 'url')
    },
  }

  const setSession = async function(response) {
    const res = response.clone()
    const data = await res.json()

    if (data) {
      const event = [
        {
          name: 'segmentToken',
          value: data.segmentToken,
        },
        {
          name: 'sessionToken',
          value: data.sessionToken,
        },
      ]

      const sessionConnection = dbConnections.session()
      try {
        await database.set(sessionConnection, sessionDB, sessionTable, event)
        database.close(sessionConnection)
      } catch (err) {
        handleErrorDB(sessionDB, sessionTable, err)
      }
    }
  }

  const getClient = async function() {
    const client = await self.clients.matchAll({
      type: 'window',
    })
    return client.find(client => client.visibilityState === 'visible')
  }

  // Validate cache based on session

  const validateCache = async function(request) {
    try {
      const regexMatch = request.url.match(/.*(?:no-cache|_secure).*/)

      const sessionConnection = dbConnections.session()

      const cacheKeyConnection = dbConnections.cacheKey()

      const cacheKeyValue = await database.get(
        cacheKeyConnection,
        cacheKeyDB,
        cacheKeyTable,
        request.url.split('?')[0]
      )
      let cacheKeyIsEqual = true

      const segmentValue = await database.get(
        sessionConnection,
        sessionDB,
        sessionTable,
        regexMatch ? 'sessionToken' : 'segmentToken'
      )

      const segmentKV = [
        {
          url: request.url.split('?')[0],
          value: segmentValue ? segmentValue.value : null,
        },
      ]
      if (cacheKeyValue && segmentValue) {
        if (
          !(
            cacheKeyValue.value === segmentValue.value &&
            (cacheKeyValue.value != null && segmentValue.value != null)
          )
        ) {
          cacheKeyIsEqual = false
          database
            .set(cacheKeyConnection, cacheKeyDB, cacheKeyTable, segmentKV)
            .catch(err => handleErrorDB(cacheKeyDB, cacheKeyTable, err))
        }
      }
      database.close(cacheKeyConnection)
      database.close(sessionConnection)
      return cacheKeyIsEqual
    } catch (err) {
      handleError(err)
      return false
    }
  }

  // Remove unused querystrings

  const validateQueryString = async function(requestUrl) {
    const url = new URL(requestUrl)
    const newUrl = new URL(url.href.split('?')[0])
    if (url.search.length > 0) {
      for (const param of url.searchParams.entries()) {
        if (WHITELIST_QS_PARAMS.has(param[0].toLowerCase())) {
          newUrl.searchParams.append(param[0], param[1])
        }
      }
    }
    return newUrl
  }

  /* End Global Function */

  const debug = 'false' === 'true'

  workbox.setConfig({ debug })
  workbox.core.skipWaiting()
  workbox.core.clientsClaim()
  workbox.navigationPreload.enable()
  workbox.core.setCacheNameDetails({
    prefix: 'VTEX-',
  })

  workbox.googleAnalytics.initialize({
    parameterOverrides: {
      cd1: 'offline',
    },
  })

  /* Cache plugins */

  const isSuccess = status => status >= 200 && status < 300
  const cacheSuccessPlugin = {
    cacheWillUpdate: async ({ response }) =>
      isSuccess(response.status) ? response : null,
  }

  const shouldCacheOrUseCache = {
    cacheWillUpdate: async ({ response }) => {
      if (response) {
        const cacheControlHeader = response.headers.get('cache-control')
        return cacheControlHeader && cacheControlHeader.includes('no-cache')
          ? null
          : response
      }
    },
    cachedResponseWillBeUsed: async ({ request, cachedResponse }) => {
      if (cachedResponse) {
        return (await validateCache(request)) ? cachedResponse : null
      }
      const client = await getClient()
      if (client && request.url.split('?')[0] === client.url.split('?')[0]) {
        return null
      }

      const filteredUrl = await validateQueryString(request.url)
      const cachedMatch = await caches.match(filteredUrl.href, {
        ignoreSearch: true,
      })
      return cachedMatch || null
    },
  }

  const apiCacheHandler = {
    cacheWillUpdate: async ({ response }) => {
      if (response.status !== 206) {
        return response
      }
      const init = {
        status: '200',
        statusText: 'OK',
        headers: response.headers,
      }
      const body = await response.text()
      return new Response(body, init)
    },
  }

  /* End Cache plugins */

  /* Fetch handlers */

  // Unused for now, but may return if it makes sense to use it anywhere else
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const defaultHandler = new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'doc',
    plugins: [cacheSuccessPlugin, shouldCacheOrUseCache],
  })

  const networkOnlyHandler = new workbox.strategies.NetworkOnly()

  const networkFirstHandler = new workbox.strategies.NetworkFirst({
    plugins: [cacheSuccessPlugin],
  })

  const apiHandler = new workbox.strategies.NetworkFirst({
    cacheName: 'api',
    plugins: [cacheSuccessPlugin, apiCacheHandler],
  })

  const defaultFallbackStrategy = async function({ event, url }) {
    if (url.host === location.hostname) {
      try {
        const response = await networkFirstHandler.handle({ event })

        const key = 'segmentToken'

        const sessionConnection = dbConnections.session()

        const cacheKeyConnection = dbConnections.cacheKey()

        const segment = await database.get(
          sessionConnection,
          sessionDB,
          sessionTable,
          key
        )
        database.close(sessionConnection)
        if (response && segment) {
          const arrKV = [
            {
              url: response.url.split('?')[0],
              value: segment.value,
            },
          ]
          database.set(cacheKeyConnection, cacheKeyDB, cacheKeyTable, arrKV)
          database.close(cacheKeyConnection)
        }
        return response
      } catch (err) {
        handleErrorDB(sessionDB, sessionTable, err)
      }
    } else {
      return networkOnlyHandler.handle({ event })
    }
  }

  const sessionStrategy = async function({ event }) {
    if (event.request.method === 'POST') {
      return fetch(event.request).then(response => {
        setSession(response)
        return response
      })
    }
    return fetch(event.request)
  }

  const imagesHandler = new workbox.strategies.CacheFirst({
    cacheName: 'img',
    plugins: [
      cacheSuccessPlugin,
      new workbox.expiration.Plugin({
        maxEntries: 500,
        // Cache for a maximum of 30 days
        maxAgeSeconds: 30 * 24 * 60 * 60,
      }),
    ],
  })

  const rcHandler = function(event) {
    const fallbackResponse = new Response('', {
      headers: {
        'Access-Control-Allow-Headers':
          'Content-Type, Authorization, Content-Length, X-Requested-With',
        'Access-Control-Allow-Methods': 'GET,POST,OPTIONS',
        'Access-Control-Allow-Origin': '*',
        Date: new Date(Date.now()).toUTCString(),
        'X-Powered-By': 'Express',
        Connection: 'keep-alive',
      },
    })
    event.respondWith(fallbackResponse)
    fetch(event.request)
  }

  const fontsHandler = new workbox.strategies.CacheFirst({
    cacheName: 'fonts',
    plugins: [
      cacheSuccessPlugin,
      new workbox.expiration.Plugin({
        maxEntries: 20,
        // Cache for a maximum of 1 year
        maxAgeSeconds: 60 * 60 * 24 * 365,
      }),
    ],
  })

  const rcAssetsHandler = new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'assets',
    plugins: [
      cacheSuccessPlugin,
      new workbox.expiration.Plugin({
        // cache for a maximum of 1 year
        maxAgeSeconds: 60 * 60 * 24 * 365,
      }),
    ],
  })

  const assetsHandler = new workbox.strategies.CacheFirst({
    cacheName: 'assets',
    plugins: [
      cacheSuccessPlugin,
      new workbox.expiration.Plugin({
        maxEntries: 800,
        maxAgeSeconds: 60 * 60 * 24 * 30,
      }),
    ],
  })

  const startUrlHandler = function(event) {
    return fetch(event.request)
  }

  /* End Fetch handlers */

  /* Routes */

  workbox.routing.registerRoute(
    /.*(?:\/vtexid\/|\/checkout\/).*/,
    networkOnlyHandler
  )

  workbox.routing.registerRoute(/(?:\/admin\/).*/, networkFirstHandler)

  workbox.routing.registerRoute(
    /(?!.*files).*(segments|session).*/,
    sessionStrategy,
    'POST'
  )

  // Default response handlink fetch event
  workbox.routing.registerRoute(/.*(?:rc\.vtex\.com).*/, ({ event }) =>
    rcHandler(event)
  )

  // Linked no cache
  // https://regex101.com/r/SkZr63/2
  workbox.routing.registerRoute(
    /.+(?:\/private\/assets(?:\/v\d+)?\/linked\/).+/,
    networkFirstHandler
  )

  // Cache Image Files (Cache First)
  workbox.routing.registerRoute(
    /(?:.+vteximg.+|\/.+)\.(?:png|jpg|jpeg|svg|gif).*/,
    imagesHandler
  )

  // Cache fonts for 1 year (Cache First)
  workbox.routing.registerRoute(
    /.*(?:\.(font|woff2|woff|eot|ttf|svg)|fonts\.googleapis|font-awesome).*/,
    fontsHandler
  )

  // RC assets
  // https://regex101.com/r/2ecw6C/1
  workbox.routing.registerRoute(
    /(?:.+io\.vtex\.com\.br\/rc\/rc\.js)/,
    rcAssetsHandler
  )

  // Assets
  // https://regex101.com/r/Nn89J3/4
  workbox.routing.registerRoute(
    /(?:.+io\.vtex\.com\.br(?!\/rc\/rc\.js)|.+\.vteximg\..*?\/public\/assets(?:\/v\d+)?\/published\/).+/,
    assetsHandler
  )

  // API
  workbox.routing.registerRoute(
    /(?:\/api|\/buscapagina|https:\/\/api\.vtex\.com|\/graphql\/public\/|\/_v\/(?:segment|private|public)\/.*\/).*/,
    apiHandler
  )

  // Google analytics
  workbox.routing.registerRoute(
    ({ url }) => url.hostname === 'www.google-analytics.com',
    networkFirstHandler
  )

  // Start url from manifest
  workbox.routing.registerRoute(/(\?homescreen)/, startUrlHandler)

  // Cache all other request that not match before routes (Stale While Revalidate with segment key validator)
  workbox.routing.registerRoute(/(\/|https:\/\/).*/, defaultFallbackStrategy)
})()�A�Eo��   Ę�y�7      �  eG        �ܼ҇ / 1  HTTP/1.1 200 status:200 date:Sat, 28 Mar 2020 22:14:21 GMT content-type:text/javascript vary:Accept-Encoding vary:Accept-Encoding,x-vtex-locale,x-vtex-segment,x-vtex-session,Cookie server:VTEX IO content-encoding:gzip x-router-cache:MISS service-worker-allowed:/ server-timing:0.pwa-gql#;dur=0, x-request-id:1e1457f47f9a409d9817a0bc44857117 x-vtex-router-version:6.1.2 x-vtex-router-elapsed-time:00:00:00.0040558 x-vtex-backend-elapsed-time:00:00:00.0015365 x-vtex-backend:vtex.pwa-graphql@1.15.1 x-vtex-io-cluster-id:stores-1b x-envoy-upstream-service-time:5        _  0�[0�C�y?��sg�g/>V��K40	*�H�� 0J10	UUS10U
Let's Encrypt1#0!ULet's Encrypt Authority X30200312092148Z200610092148Z010Uwww.motorola.co.uk0�"0	*�H�� � 0�
� ���Pش۞�-��zU;a7����JQ�5�۸������J��!Q#��=X�c��Չ�'{��L	���Ҹc��i�T�er(�;U<�^<�� �.p�Q��V��+2:�j�<,M��jY~,Z��D/k�Xh@�E4�I������Y	J���;�lE��p�B��ŏ7�St͈��s���!L'6�1j�ä�~�+k�f�#��sj�
����&䇺���߁��S��p�)R.?�ćL2nH���}�o`vA ��f0�b0U��0U%0++0U�0 0U�<��������z��*0U#0��Jjc}ݺ��9��Ee���0o+c0a0.+0�"http://ocsp.int-x3.letsencrypt.org0/+0�#http://cert.int-x3.letsencrypt.org/0U0�www.motorola.co.uk0LU E0C0g�07+��0(0&+http://cps.letsencrypt.org0�
+�y���� � v �\�}h���#Ǻ�W|W��j�a:iӢ  p�C�f   G0E o���D<���E��[Px��e�>O	�����! �u��O#86e@�D�0�"�hm#��r������ u oSv�1�1ؙ �Q�w�� )���7�  p�C��   F0D  �j;d}��֔q�Ds��2����|0��g si�}O>��P��K�+�<���Qy��1I�۝�0	*�H�� � lP��*�^�9�����Y>,B�:�69h��r�L�Ɛ%O��Գ���4Q�r���[�����Q���z���������r��X�����c���L[�T�4����s��0b:OWm��h�7�"m�Wt5�nI,]>�-Û���ti ������|��E����_��-6IPD�;zY;�4�G�ޫ�K0m���-BQZî�y��v�	r�����a�_&��z�wT��/����q�§�|\V+� �  0��0�z�
AB  S�sj��0	*�H�� 0?1$0"U
Digital Signature Trust Co.10UDST Root CA X30160317164046Z210317164046Z0J10	UUS10U
Let's Encrypt1#0!ULet's Encrypt Authority X30�"0	*�H�� � 0�
� ���Z�.G�r]7��hc0��5&%὾5�p�/��KA���5X�*�h���u���bq�y�`ב����xgq�i������`<H�~�Mw$�GZ��7 ��{���J�A�6����m<�h�#*B��tg����Ra?e邇���V����?������k�}�+�e��6u�k�J��Ix/��O* %)�t��1͏18���3�C��0�y1=-6����3j�91ůčd3���)����}Ó ��}0�y0U�0� 0U��0+s0q02+0�&http://isrg.trustid.ocsp.identrust.com0;+0�/http://apps.identrust.com/roots/dstrootcax3.p7c0U#0�ħ��{,q���K�u��`��0TU M0K0g�0?+��000.+"http://cps.root-x1.letsencrypt.org0<U50301�/�-�+http://crl.identrust.com/DSTROOTCAX3CRL.crl0U�Jjc}ݺ��9��Ee���0	*�H�� � �3��cX8��	U�vV�pH�iG'{�$��ZJ)7$tQbh�͕pg����N(Q͛讇��غZ�����jj�>W#��b���ʷ?�
H����eb��T�*� ��������2���w��ye+�(�:R��R._���3�wl�@�2��\A�tl[]
_3�M��8�/{,b�٣�o%/��F=�~�z���zm�%�����/X�/,h&�K��ڟ���CJDNosz(ꤪn{L}����D����4[�B  N  0�J0�2�D���֣'��09�.�@k0	*�H�� 0?1$0"U
Digital Signature Trust Co.10UDST Root CA X30000930211219Z210930140115Z0?1$0"U
Digital Signature Trust Co.10UDST Root CA X30�"0	*�H�� � 0�
� ߯�P�W��be������,k0�[���}�@����3vI*�?!I��N�>H�e���!e�*�2����w�{��������s.z2��~�0���*8�y
1�P��e߷Qc���a�Ka��Rk���K(�H���	�>.� �[�*��.�p��%�tr�{j�4�0���h{T�֮�Z��=d���߿�Ap�r�&�8U9C���\@��՚����%�����:��n�>.�\�<փi��*�)W��=���] �B0@0U�0�0U�0Uħ��{,q���K�u��`��0	*�H�� � �,� \��(f7:���?K�	�� ]��YD�>>��K�t���t~���K� D����}��j�����Nݷ :�pI�������˔:^H�X_Z�񱭩�Y��n������f3�Y[��sG�+̙�7H��VK��r2���D�SrmC�&H�Rg�X��gvqx��V9$1���Z0G��P�	� �dc`������}���=2�e��|�Wv�Ŷ(9�e��w�j
�wؑ��)�
55      `    151.80.204.60   �     h2          0�1��<?��"�u����J���ltXX���A�Eo��   ��a%�      