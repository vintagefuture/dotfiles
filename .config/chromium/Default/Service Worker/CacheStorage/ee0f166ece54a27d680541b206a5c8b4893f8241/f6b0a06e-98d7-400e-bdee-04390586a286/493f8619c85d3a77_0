0\r�m��   c   �t2;    https://www.thepolyglotdeveloper.com/2017/03/nginx-reverse-proxy-containerized-docker-applications/<!doctype html><html lang="en-us"><head><meta http-equiv="content-type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width,initial-scale=1"><title>Use NGINX As A Reverse Proxy To Your Containerized Docker Applications</title><meta property="og:title" content="Use NGINX As A Reverse Proxy To Your Containerized Docker Applications"><meta name="twitter:site" content="@polyglotdev"><meta name="twitter:card" content="summary_large_image"><meta property="og:site_name" content="The Polyglot Developer"><link rel="icon" type="image/png" href="/favicon.png" sizes="192x192"><link rel="apple-touch-icon" href="/apple-touch-icon.png"><link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png"><meta property="og:image" content="https://www.thepolyglotdeveloper.com/the-polyglot-developer.png"><meta property="og:url" content="https://www.thepolyglotdeveloper.com/2017/03/nginx-reverse-proxy-containerized-docker-applications/"><meta name="description" content="Learn how to create several web application containers and access them behind an NGINX reverse proxy that is also a Docker container."><meta property="og:description" content="Learn how to create several web application containers and access them behind an NGINX reverse proxy that is also a Docker container."><meta name="keywords" content="operations,docker"><meta name="author" content="Nic Raboy"><meta property="article:author" content="Nic Raboy"><meta name="twitter:creator" content="@nraboy"><meta property="og:type" content="article"><script type="application/ld+json">{
                "@context": "https://schema.org",
                "@type": "BlogPosting",
                "headline": "Use NGINX As A Reverse Proxy To Your Containerized Docker Applications",
                "url": "https:\/\/www.thepolyglotdeveloper.com\/2017\/03\/nginx-reverse-proxy-containerized-docker-applications\/",
                "datePublished": "2017-03-15 07:00:00 -0700 -0700",
                "dateModified": "2017-03-15 07:00:00 -0700 -0700",
                "description": "Learn how to create several web application containers and access them behind an NGINX reverse proxy that is also a Docker container.",
                "author": {
                    "@type": "Person",
                    
                        
                        "name": "Nic Raboy"
                    
                },
                
                    "image": "https:\/\/www.thepolyglotdeveloper.com\/the-polyglot-developer.png",
                
                "publisher": {
                    "@type": "Organization",
                    "name": "The Polyglot Developer",
                    "logo": {
                        "@type": "ImageObject",
                        
                            "url": "https:\/\/www.thepolyglotdeveloper.com\/the-polyglot-developer.png",
                            "width": "1200",
                            "height": "627"
                        
                    }
                }
            }</script><meta name="msvalidate.01" content="FF7C3D67A1C531B8934A757318B8F574"><meta name="google-site-verification" content="rzTcZZFOcvza3CORUSHbrJ9LRQYwXKBzGqnTOq3hjgw"><meta name="yandex-verification" content="476848941c4c4b89"><link rel="canonical" type="text/html" href="https://www.thepolyglotdeveloper.com/2017/03/nginx-reverse-proxy-containerized-docker-applications/" title="The Polyglot Developer"><link rel="manifest" href="/manifest.json"><meta name="theme-color" content="#931C22"><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap"><link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css"><link rel="stylesheet" href="/css/chroma/monokai.min.css"><link rel="stylesheet" href="/css/custom.min.css"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><script>(adsbygoogle=window.adsbygoogle||[]).push({google_ad_client:"ca-pub-8504140913961629",enable_page_level_ads:!1})</script></head><body><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PD9XZQF" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><div id="wrapper"><nav class="navbar navbar-default navbar-fixed-top"><div class="container"><div class="navbar-header"><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="/"><span><img id="logo-icon" src="/images/logo-icon.svg" alt="The Polyglot Developer"></span>The Polyglot Developer</a></div><div id="navbar" class="navbar-collapse collapse"><ul class="nav navbar-nav navbar-right"><li><a href="/">About</a></li><li><a href="/blog/">Blog</a></li><li><a href="https://courses.thepolyglotdeveloper.com">Courses</a></li><li><a href="/resources/">Resources</a></li></ul></div></div></nav><div><div id="content-wrapper" class="container"><div id="HwbCBpzMftGW"><div class="row"><div class="col-md-12">Our website is made possible by displaying online advertisements to our visitors. Please consider supporting us by disabling your ad blocker.</div></div></div><div class="row"><div id="core-content" class="col-md-9"><h1>Use NGINX As A Reverse Proxy To Your Containerized Docker Applications</h1><div class="content-meta"><ul><li><img src="/fontawesome/calendar.svg" style="width:.9em" alt="Publish Date">March 15, 2017</li><li><img src="/fontawesome/user.svg" style="width:.9em" alt="Author"> Nic Raboy</li><li><img src="/fontawesome/folder.svg" style="width:.9em" alt="Categories"><a href="https://www.thepolyglotdeveloper.com/categories/operations/">Operations</a></li></ul></div><div id="social-sharing" class="row"><div class="col-md-12"><div class="content-share"><a target="_blank" rel="noopener noreferrer" class="fab" href="https://twitter.com/share?url=https%3a%2f%2fwww.thepolyglotdeveloper.com%2f2017%2f03%2fnginx-reverse-proxy-containerized-docker-applications%2f&amp;text=Use%20NGINX%20As%20A%20Reverse%20Proxy%20To%20Your%20Containerized%20Docker%20Applications" title="Twitter"><img src="/fontawesome/twitter.svg" alt="Twitter"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="https://www.facebook.com/sharer.php?u=https%3a%2f%2fwww.thepolyglotdeveloper.com%2f2017%2f03%2fnginx-reverse-proxy-containerized-docker-applications%2f" title="Facebook"><img src="/fontawesome/facebook-f.svg" alt="Facebook"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="https://reddit.com/submit?url=https%3a%2f%2fwww.thepolyglotdeveloper.com%2f2017%2f03%2fnginx-reverse-proxy-containerized-docker-applications%2f&amp;title=Use%20NGINX%20As%20A%20Reverse%20Proxy%20To%20Your%20Containerized%20Docker%20Applications" title="Reddit"><img src="/fontawesome/reddit.svg" alt="Reddit"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3a%2f%2fwww.thepolyglotdeveloper.com%2f2017%2f03%2fnginx-reverse-proxy-containerized-docker-applications%2f" title="LinkedIn"><img src="/fontawesome/linkedin-in.svg" alt="LinkedIn"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="https://news.ycombinator.com/submitlink?u=https%3a%2f%2fwww.thepolyglotdeveloper.com%2f2017%2f03%2fnginx-reverse-proxy-containerized-docker-applications%2f&amp;t=Use%20NGINX%20As%20A%20Reverse%20Proxy%20To%20Your%20Containerized%20Docker%20Applications" title="Hacker News"><img src="/fontawesome/hacker-news.svg" alt="Hacker News"></a></div></div></div><p>You might have noticed that I’m doing quite a bit of <a href="https://www.docker.com/">Docker</a> related articles lately. This is because I’ve been exploring it as an option for the future of my personal web applications. As of right now I’m serving several web applications on <a href="https://www.digitalocean.com/?refcode=ee4903914bb4">Digital Ocean</a> under a single Apache instance. As requests come into my server, Apache routes them to the appropriate application via virtual hosts. Each application is a different directory on the virtual private server (VPS). If I were to containerize each application, things would behave a bit differently. I would need to set up a reverse proxy to route each request to a different container on the host.</p><p>While Apache can work as a reverse proxy, there are other options that work way better. For example NGINX is known for being an awesome reverse proxy solution. We’re going to see how to create several web application containers and route between them with an NGINX reverse proxy container.</p><p>For simplicity we’re going to use two stock Docker images straight from Docker Hub and one custom image, the custom image being our reverse proxy. To see where we’re heading, create a <strong>docker-compose.yml</strong> file with the following:</p><div class="highlight"><pre class="chroma"><code class="language-fallback" data-lang="fallback">version: &#39;2&#39;

services:
    reverseproxy:
        image: reverseproxy
        ports:
            - 8080:8080
            - 8081:8081
        restart: always

    nginx:
        depends_on:
            - reverseproxy
        image: nginx:alpine
        restart: always

    apache:
        depends_on:
            - reverseproxy
        image: httpd:alpine
        restart: always
</code></pre></div><p>The <code>reverseproxy</code> service will use an image that we’ll create shortly. The <code>nginx</code> and <code>apache</code> services will use each of their respective images and depend on the <code>reverseproxy</code> service being available.</p><p>Only ports in the <code>reverseproxy</code> service are exposed to the host machine. This is actually a good thing because this means that the host won’t be able to communicate to any of the exposed services of our other containers directly. In a production environment, you’ll probably want your reverse proxy to use port 80 and 443, but since I’m doing everything locally without server names, I have to differentiate each of my web services by port. After all, you can’t expect http://localhost:80 to know where to go. Only a server name like example1.com and example2.com can take care of that.</p><p>So what does our custom image look like?</p><p>The custom image representing our reverse proxy will need a <strong>Dockerfile</strong> file as well as a custom NGINX configuration file. I’ll call it <strong>nginx.conf</strong>, but it doesn’t really matter what you call it.</p><p>Open the <strong>Dockerfile</strong> and include the following:</p><div class="highlight"><pre class="chroma"><code class="language-fallback" data-lang="fallback">FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf
</code></pre></div><p>This custom image will have a base Alpine Linux image running NGINX. During the build process, our configuration file will be copied into the image.</p><p>Open the <strong>nginx.conf</strong> file or whatever you called it, and include the following:</p><div class="highlight"><pre class="chroma"><code class="language-fallback" data-lang="fallback">worker_processes 1;

events { worker_connections 1024; }

http {

    sendfile on;

    upstream docker-nginx {
        server nginx:80;
    }

    upstream docker-apache {
        server apache:80;
    }

    server {
        listen 8080;

        location / {
            proxy_pass         http://docker-nginx;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
    }

    server {
        listen 8081;

        location / {
            proxy_pass         http://docker-apache;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
    }

}
</code></pre></div><p>The magic happens in the above file.</p><p>First of all, notice the <code>upstream</code> declarations. We have two upstreams because we have two web applications. The <code>server</code> inside each of the upstreams represents where to find each of the applications.</p><div class="highlight"><pre class="chroma"><code class="language-fallback" data-lang="fallback">upstream docker-nginx {
    server nginx:80;
}
</code></pre></div><p>The hostname must match the service name found in the <strong>docker-compose.yml</strong> file. By default, NGINX and Apache web servers broadcast on port 80, but if you’ve changed it, make sure to update the upstream server port.</p><p>After defining the upstream servers we need to tell NGINX how to listen and how to react to requests. Take for example the following:</p><div class="highlight"><pre class="chroma"><code class="language-fallback" data-lang="fallback">server {
    listen 8080;

    location / {
        proxy_pass         http://docker-nginx;
        proxy_redirect     off;
        proxy_set_header   Host $host;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Host $server_name;
    }
}
</code></pre></div><p>If we try to access the host machine via port 8080, NGINX will act as a reverse proxy and serve whatever is in the <code>proxy_pass</code> definition. In the above scenario we have <code>docker-nginx</code> which is the name of one of our upstream servers. This means the NGINX service will be served.</p><p>In production you might have something like this:</p><div class="highlight"><pre class="chroma"><code class="language-fallback" data-lang="fallback">listen 80;
server_name example1.com;
</code></pre></div><p>Before we can launch our containers, we need to build our reverse proxy image. This can easily be accomplished by executing the following command:</p><div class="highlight"><pre class="chroma"><code class="language-fallback" data-lang="fallback">docker build -t reverseproxy ./path/to/directory/with/dockerfile/and/nginx.conf
</code></pre></div><p>The <strong>docker-compose.yml</strong> file expects an image by the name of <code>reverseproxy</code> so that is what we’re building. The <strong>Dockerfile</strong> and <strong>nginx.conf</strong> file should exist in the same location.</p><p>So let’s test out what we have.</p><p>We’re using the <strong>docker-compose.yml</strong> file, but we don’t truly have to. It is just convenient for this example. Execute the following command via your shell:</p><div class="highlight"><pre class="chroma"><code class="language-fallback" data-lang="fallback">docker-compose up -d
</code></pre></div><p>When complete, we should have three containers deployed, two of which we cannot access directly. From the web browser, navigate to http://localhost:8080. This will hit the NGINX reverse proxy which will in turn load the NGINX web application. Now try to navigate to http://localhost:8081 in your web browser. The NGINX reverse proxy will be hit and the Apache web application will be loaded.</p><p>Not bad right?</p><h2 id="conclusion">Conclusion</h2><p>You just saw how to deploy several web application containers with <a href="https://www.docker.com/">Docker</a> and control them with an NGINX reverse proxy. Using a reverse proxy is useful if you want to containerize your applications and still have access to them. Remember you can’t access all of them via port 80 or 443 on the host.</p><p>If you’re like me and use <a href="https://www.digitalocean.com/?refcode=ee4903914bb4">Digital Ocean</a>, this strategy is perfect for keeping control of your applications. You wouldn’t be restricted to Digital Ocean, for example, you could also use <a href="https://www.linode.com/?r=80c3d44cde0ac7fdc84494c29c9c67c4efefd489">Linode</a> or anything else.</p><div class="content-tags"><ul><li><a href="https://www.thepolyglotdeveloper.com/tags/docker/">docker</a></li></ul></div><div id="author-box"><div class="row"><div class="col-xs-3"><img src="/media/authors/nraboy.jpg" alt="Nic Raboy" width="100%"></div><div class="col-xs-9"><h3>Nic Raboy</h3><p>Nic Raboy is an advocate of modern web and mobile development technologies. He has experience in Java, JavaScript, Golang and a variety of frameworks such as Angular, NativeScript, and Apache Cordova. Nic writes about his development experiences related to making web and mobile development easier to understand.</p></div></div></div><div class="row"><div class="col-md-12 content-card"><div id="disqus_thread"></div><script>!function(){if("localhost"!=window.location.hostname){var e=document.createElement("script");e.type="text/javascript",e.async=!0;e.src="//nraboy.disqus.com/embed.js",(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(e)}else document.write("Disqus comments are unavailable while serving on localhost or 127.0.0.1")}()</script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript></div></div></div><div class="col-md-3"><div class="row widget-wrapper"><div class="col-md-12"><div class="widget"><h3 class="widget-title">Search</h3><div class="widget-content"><form action="https://google.com/search" method="get" accept-charset="UTF-8" class="search-form"><div class="input-group"><input class="form-control" type="search" name="q" aria-label="Search Query" required> <span class="input-group-btn"><button class="btn btn-primary" type="submit" aria-label="Search"><img src="/fontawesome/search.svg" width="14" alt="Search"></button></span></div><input type="hidden" name="q" value="site:https://www.thepolyglotdeveloper.com"></form></div></div></div></div><div class="row widget-wrapper"><div class="col-md-12"><div class="widget"><h3 class="widget-title">Follow Us</h3><div class="widget-content social-media"><a target="_blank" rel="noopener noreferrer" class="fab" href="https://www.twitter.com/polyglotdev" title="Twitter"><img src="/fontawesome/twitter.svg" alt="Twitter"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="https://www.facebook.com/thepolyglotdeveloper/" title="Facebook"><img src="/fontawesome/facebook-f.svg" alt="Facebook"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="https://www.youtube.com/user/NicRaboy" title="YouTube"><img src="/fontawesome/youtube.svg" alt="YouTube"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="https://www.linkedin.com/company/thepolyglotdeveloper" title="LinkedIn"><img src="/fontawesome/linkedin-in.svg" alt="LinkedIn"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="https://discord.gg/qMc2DmA" title="Discord"><img src="/fontawesome/discord.svg" alt="Discord"></a></div></div></div></div><div class="row widget-wrapper"><div class="col-md-12"><div class="widget-other"><div class="widget-content"><ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-8504140913961629" data-ad-slot="8181093570" data-ad-format="auto" data-full-width-responsive="true"></ins><div class="KAZFMzEvH8"><a target="_blank" href="https://www.thepolyglotdeveloper.com/sponsors/"><img src="/media/sponsors/thepolyglotdeveloper/placeholder-sidebar-2.svg" alt="The Polyglot Developer" width="100%" loading="lazy"></a></div></div></div></div></div><div class="row widget-wrapper"><div class="col-md-12"><div class="widget"><h3 class="widget-title">Subscribe</h3><div class="widget-content"><p>Subscribe to the newsletter for monthly tips and tricks on subjects such as mobile, web, and game development.</p><script async data-uid="abaebde006" src="https://the-polyglot-developer.ck.page/abaebde006/index.js"></script></div></div></div></div><div id="recent-posts" class="row"><div class="col-md-12"><div class="widget"><h3 class="widget-title">Recent Posts</h3><div class="widget-list"><ul><li><a href="/2020/06/live-streaming-on-twitch-for-beginners/">Live-Streaming on Twitch for Beginners, Released</a></li><li><a href="/2020/06/tpdp-e37-writing-tests-development-project/">TPDP E37: Writing Tests in a Development Project</a></li><li><a href="/2020/05/location-geofencing-mongodb-stitch-mapbox/">Location Geofencing with MongoDB, Stitch, and Mapbox</a></li><li><a href="/2020/05/boost-performance-obs-twitch-streams-mac-windows/">Boost the Performance of OBS Twitch Streams on Mac and Windows</a></li><li><a href="/2020/05/tpdp-e36-machine-learning-ai-data-science/">TPDP Episode #36: Machine Learning, AI, and Data Science</a></li></ul></div></div></div></div><div class="row widget-wrapper"><div class="col-md-12"><div class="widget-other"><div class="widget-content"><ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-8504140913961629" data-ad-slot="9430418454" data-ad-format="auto" data-full-width-responsive="true"></ins><div class="KAZFMzEvH8"><a target="_blank" href="https://www.thepolyglotdeveloper.com/sponsors/"><img src="/media/sponsors/thepolyglotdeveloper/placeholder-sidebar-1.svg" alt="The Polyglot Developer" width="100%" loading="lazy"></a></div></div></div></div></div><div class="row widget-wrapper"><div class="col-md-12"><div class="widget"><h3 class="widget-title">Support This Site</h3><div class="widget-content social-media"><p>If you found this developer resource helpful, please consider supporting it through the following options:</p><a target="_blank" rel="noopener noreferrer" class="fab" href="https://paypal.me/nraboy" title="PayPal"><img src="/fontawesome/paypal.svg" alt="PayPal"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="https://amzn.com/w/1YU2424SGEF1B" title="Amazon"><img src="/fontawesome/amazon.svg" alt="Amazon"></a><a target="_blank" rel="noopener noreferrer" class="fas" href="https://cash.me/$nraboy" title="Square Cash"><img src="/fontawesome/dollar-sign.svg" alt="Square Cash"></a><a target="_blank" rel="noopener noreferrer" class="fab" href="bitcoin:15sf3hLC4JqdBzBxSZ6PWFhbVy85Wo9Whd" title="Bitcoin"><img src="/fontawesome/bitcoin.svg" alt="Bitcoin"></a></div></div></div></div></div></div></div></div><footer><div id="footer-one" class="container-fluid"><div class="container"><div class="row"><div class="col-md-12"><div><ul><li><a href="/privacy-policy/">Privacy Policy</a></li><li><a href="https://newsletter.thepolyglotdeveloper.com/">Newsletter</a></li><li><a href="/sponsors/">Sponsor</a></li><li><a href="/hire-me/">Hire Me</a></li></ul></div></div></div></div></div><div id="footer-two" class="container-fluid"><div class="container"><div class="row"><div class="col-md-4"><div>Copyright &copy; <a href="https://www.thepolyglotdeveloper.com">The Polyglot Developer</a> 2020</div></div></div></div></div></footer></div><script src="/js/jquery/jquery.min.js"></script><script src="/js/bootstrap/bootstrap.min.js"></script><script src="/js/gtm.min.js"></script><script src="https://apis.google.com/js/platform.js"></script><script>!function(e,a,t,n,s,g,o){e.GoogleAnalyticsObject=s,e.ga=e.ga||function(){(e.ga.q=e.ga.q||[]).push(arguments)},e.ga.l=1*new Date,g=a.createElement(t),o=a.getElementsByTagName(t)[0],g.async=1,g.src="https://www.google-analytics.com/analytics.js",o.parentNode.insertBefore(g,o)}(window,document,"script",0,"ga"),ga("create","UA-27068716-23","auto"),ga("set","dimension1","post"),ga("set","dimension2","Nic Raboy"),ga("send","pageview")</script><script>$(document).ready(() => {
                $("#core-content > p:nth-of-type(3)").after("<ins class='adsbygoogle' style='display: block; text-align:center; margin-bottom: 20px' data-ad-client='ca-pub-8504140913961629' data-ad-slot='4183247972' data-ad-layout='in-article' data-ad-format='fluid'></ins>");
                $("#core-content > p:nth-of-type(3)").after("<div class='KAZFMzEvH8'><p><a target='_blank' href='https://www.thepolyglotdeveloper.com/sponsors/'><img src='/media/sponsors/thepolyglotdeveloper/placeholder-inline-1.svg' alt='The Polyglot Developer' width='100%' /></a></p></div>");
            });</script><script>$(document).ready(() => {
                for(var i = 1; i < $(".adsbygoogle").length; i++) {
                    
                    (adsbygoogle = window.adsbygoogle || []).push({});
                }
            });</script><script async data-uid="0e60b2f5b5" src="https://f.convertkit.com/0e60b2f5b5/f3695a8de7.js"></script><script src="/js/dfp.js"></script><script type="text/javascript">$(document).ready(() => {
        if(!document.getElementById('aUpkOxWdYNCM')) {
            if(document.getElementById('HwbCBpzMftGW')) {
                document.getElementById('HwbCBpzMftGW').style.display='block';
                $(".KAZFMzEvH8").css("display", "block");
            }
        }
    });</script><script>if("serviceWorker" in navigator) {
        window.addEventListener("load", () => {
            navigator.serviceWorker.register("/sw.js").then(swReg => {}).catch(err => {
                console.error('Service Worker Error', err);
            });
        });
    }</script></body></html>�A�Eo��   ��,*Kc      
�
GET�
Accept|text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Upgrade-Insecure-Requests1v

User-AgenthMozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36��  "
age1"3
cache-control"public, max-age=0, must-revalidate"
content-encodingbr"
content-length6756"(
content-typetext/html; charset=UTF-8"%
dateFri, 12 Jun 2020 17:14:33 GMT"1
etag)"e48ddc5acd91795cd060a9b64eee5665-ssl-df""
serverNetlify"
status200"-
strict-transport-securitymax-age=31536000"
varyAccept-Encoding"@
x-nf-request-id-a4f42671-24ab-4a53-bdfa-463084a94817-143218960�������Bchttps://www.thepolyglotdeveloper.com/2017/03/nginx-reverse-proxy-containerized-docker-applications/H Ӝ�����@��ʣ@Z��ն.�,��2�?�G�� �Y
�=�A�Eo��   Nf;�K      